# Ansible role to collection

## What is a collection

A collection is just a directory structure and metadata used by Ansible tooling
to managed shared content. It uses all the same Ansible primitives you already
know (roles, modules, playbooks) but provides a consistent way to package all of
them together.

Collections consist of a namespace, name, and semantic version number. They follow
the same installation workflow as older style "standalone roles" (either using `ansible-galaxy`
explicitly from the command line, or implicityly through a `requirements.yml`).

The directory structure of a collection looks like this:

```
my_namespace/
  my_collection/
    docs/
    playbooks/
    plugins/
      modules/
    roles/
    tests/
    galaxy.yml
```

## Migrating a "standalone role" into a collection

See the [existing documentation](https://docs.ansible.com/ansible/latest/dev_guide/migrating_roles.html#migrating-a-role-to-a-collection).

## Calling collection based roles and modules from a playbook

You just need to prefix the role invocation with the namespace and collection. Compare
the [standalone role `playbook.yml`](https://gitlab.com/evanstoner/ansible-role-to-collection/-/blob/main/standalone-role/playbook.yml) with the [collection role `playbook.yml`](https://gitlab.com/evanstoner/ansible-role-to-collection/-/blob/main/collection-role/playbook.yml).
